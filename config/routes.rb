Rails.application.routes.draw do
  root 'home#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  get '/profil', to: 'users#edit', as: :edit_profil
  patch '/profil', to: 'users#update', as: :profil

  get '/login', to: 'sessions#new', as: :new_session
  post '/login', to: 'sessions#create', as: :sessions
  delete '/logout', to: 'sessions#destroy', as: :session

  resources :passwords, only: [:new, :create, :edit, :update]

  resources :users, only: [:new, :create] do
    member do
      get 'confirm'
    end
  end
end
