class User < ApplicationRecord

  attr_accessor :avatar_file

  has_secure_password
  has_secure_token :confirmation_token
  has_secure_token :recover_password

  after_save :avatar_upload

  after_destroy_commit :avatar_destroy

  validates :username, format: { with: /\A[a-zA-Z0-9_]{2,20}\z/, message: "ne doit contenir que des caractères alpha numériques ou des underscore" }, uniqueness: { case_sensitive: false }
  validates :email, format: { with: /\A[^@\s]+@([^@.\s]+\.)+[^@.\s]+\z/ }, uniqueness: { case_sensitive: false }
  validates :avatar_file, file: { ext: [:jpg, :png] }

  def to_session
    { id: id }
  end

  def avatar_url
    '/' + [self.class.name.downcase.pluralize, id.to_s, 'avatar.jpg'].join('/')
  end

  private

  def avatar_upload
    base_path = avatar_base_path
    if avatar_file.respond_to? :path
      path = File.join(base_path, "avatar.jpg")
      dir = File.dirname(path)
      FileUtils.mkdir_p(dir) unless Dir.exist?(dir)
      # FileUtils.cp(avatar_file.path, path)
      image = MiniMagick::Image.new(avatar_file.path) do |b|
        b.resize '150x150^'
        b.gravity 'center'
        b.crop '150x150+0+0'
      end
      image.format 'jpg'
      image.write path
      update_column(:avatar, true)
    end
  end

  def avatar_destroy
    base_path = avatar_base_path
    FileUtils.rm_r(base_path) if Dir.exist?(base_path)
  end

  def avatar_base_path
    File.join(Rails.public_path, self.class.name.downcase.pluralize, id.to_s)
  end
end
