class FileValidator < ActiveModel::EachValidator

  def validate_each(record, attribute, value)
    if value
      if value.respond_to? :path
        if !options[:ext].include?(File.extname(value.path).delete('.').to_sym)
          record.errors[attribute] << (options[:message] || "is not a good extension (#{options[:ext].join(", ")})")
        end
      else
        record.errors[attribute] << (options[:message] || 'is not a file')
      end
    end
  end
end
