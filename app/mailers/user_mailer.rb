class UserMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.confirm.subject
  #
  def confirm(user)
    @user = user

    mail to: user.email, subject: "Votre inscription sur le site #{Rails.application.config.site[:name]}"
  end

  def password(user)
    @user = user

    mail to: user.email, subject: "Password regénéré #{Rails.application.config.site[:name]}"
  end
end
