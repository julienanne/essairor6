class UsersController < ApplicationController

  skip_before_action :only_signed_in, only: [:new, :create, :confirm]

  before_action :only_signed_out, only: [:new, :create, :confirm]

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    @user.recover_password = nil
    if @user.save
      UserMailer.confirm(@user).deliver_now
      redirect_to root_path, success: "Votre compte a bien été créé, voir les mails."
    else
      render :new
    end
  end

  def edit
    @user = current_user
  end

  def update
    @user = current_user
    user_params = params.require(:user).permit(:username, :firstname, :lastname, :avatar_file, :email)
    if @user.update(user_params)
      redirect_to edit_profil_path, success: "Compte mis à jour"
    else
      render :edit
    end
  end

  def confirm
    user = User.find(params[:id])
    if user.confirmation_token == params[:token]
      user.update(confirmation_token: nil, confirmed: true)
      session[:auth] = user.to_session
      redirect_to edit_profil_path, success: "Compte confirmé"
    else
      redirect_to root_path, danger: "Désolé le token est erroné."
    end
  end

  def user_params
    params.require(:user).permit(:username, :email, :password, :password_confirmation)
  end

end
