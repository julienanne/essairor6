class HomeController < ApplicationController
  skip_before_action :only_signed_in

  def index
    @hello = Rails.env
  end
end
