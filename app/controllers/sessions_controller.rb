class SessionsController < ApplicationController
  skip_before_action :only_signed_in, only: [:new, :create]

  before_action :only_signed_out, only: [:new, :create]

  def new
  end

  def create
    user_params = params.require(:user)
    user = User.where(username: user_params[:login]).or(User.where(email: user_params[:login])).first
    if user && user.confirmed? && user.authenticate(user_params[:password])
      session[:auth] = user.to_session
      redirect_to edit_profil_path, success: "Connexion réussie"
    else
      redirect_to new_session_path, danger: "Identification incorrecte"
    end
  end

  def destroy
    session.destroy
    redirect_to root_path, success: "Vous êtes déconnecté."
  end
end
