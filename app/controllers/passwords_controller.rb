class PasswordsController < ApplicationController

  skip_before_action :only_signed_in

  before_action :only_signed_out


  def new

  end

  def create
    @user = User.find_by_email(user_params[:email])

    if @user
      @user.regenerate_recover_password
      UserMailer.password(@user).deliver_now
      redirect_to new_session_path, success: "Email envoyé avec code de regénération"
    else
      redirect_to new_password_path, danger: "Aucun utilisateur ne correspond"
    end
  end

  def edit
    @user = User.find(params[:id])
    if @user.recover_password != params[:token]
      redirect_to new_password_path, danger: "Token invalide"
    end
  end

  def update
    @user = User.find(params[:id])
    if @user.recover_password == user_params[:recover_password]
      @user.attributes = user_params.permit(:password, :password_confirmation)
      if @user.valid?
        @user.recover_password = nil
        @user.save
        session[:auth] = @user.to_session
        redirect_to root_path, success: "Mot de passe maj"
      else
        render :edit, danger: "Probleme"
      end
    else
      redirect_to new_password_path, danger: "Token invalide"
    end
  end

  def user_params
    params.require(:user)
  end

end
