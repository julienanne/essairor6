class ApplicationController < ActionController::Base
  before_action :only_signed_in

  add_flash_types :success, :danger

  helper_method :current_user, :user_signed_in?

  private
  def only_signed_in
    if !user_signed_in?
      redirect_to root_path, danger: "Vous n'êtes pas connecté"
    end
  end

  def only_signed_out
    redirect_to edit_profil_path, danger: "Vous êtes déjà connecté." if user_signed_in?
  end

  def user_signed_in?
    !current_user.nil?
  end

  def current_user
    return nil if !session[:auth] || !session[:auth]['id']
    return user if defined?(user) && user
    user = User.find_by_id(session[:auth]['id'])
  end
end
